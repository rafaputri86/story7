from django.test import TestCase, Client
from django.urls import resolve
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class TestStatusPage(TestCase) :
    def test_Apakah_ada_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('')
        self.assertFalse(response.status_code == 404)
    
    def test_func(self):
        found = resolve('/') 
        self.assertEqual(found.func, views.tampilan)
    
    def test_apakah_ada_html_status(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_konten_apakah_terdapat_nama(self):
         c = Client()
         response = c.get('')
         content = response.content.decode('utf8')
         self.assertIn ("Rafa Putri A", content)

    def test_konten_apakah_terdapat_daftar_aktivitas(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Daftar Aktivitas", content)
    
    def test_konten_apakah_terdapat_daftar_pengalaman(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Pengalaman Kepanitiaan", content)

    def test_konten_apakah_terdapat_daftar_prestasi(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Prestasi", content)

    def test_apakah_terdapat_toggle(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("toggle", content)

    def test_apakah_terdapat_accordion(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("accordion", content)
    
class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_theme_mode(self):
        self.browser.get('http://127.0.0.1:8000/')

        # find the element
        acc_1 = self.browser.find_element_by_id('ui-id-1')
        acc_2 = self.browser.find_element_by_id('ui-id-3')
        acc_3 = self.browser.find_element_by_id('ui-id-5')
    
        # test
        time.sleep(1)
      
        time.sleep(1)
        acc_2.click()
        time.sleep(1)
        acc_3.click()
        time.sleep(1)
    
        time.sleep(1)
        acc_1.click()
        time.sleep(1)

        
        time.sleep(5)
        e = self.browser.find_element_by_css_selector(".toggle")
        time.sleep(2)
        e.click()
        time.sleep(2)

        light = "rgba(150, 168, 186, 1)"
        brown = "rgba(177, 162, 150, 1)"